## Names

* Resource Dashboard?


## Dependencies

Arch

```
sudo pacman -S cmake extra-cmake-modules kwin
```

Fedora

```
sudo dnf install cmake extra-cmake-modules kf5-kconfig-devel \
    kf5-kcoreaddons-devel kf5-kwindowsystem-devel kwin-devel \
    qt5-qtbase-devel
```

Ubuntu

```
sudo apt install cmake extra-cmake-modules kwin-dev libdbus-1-dev \
    libkf5config-dev libkf5configwidgets-dev libkf5coreaddons-dev \
    libkf5windowsystem-dev qtbase5-dev
```


## Building

```
mkdir build && cd build
cmake .. \
    -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_INSTALL_PREFIX=/usr
make
sudo make install
```


# Authors

Mention where the source code comes from?
